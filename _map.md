---
date: 2022-08-23 15:03
<% tp.user.tagsCreate(tp, "todo") %>
---

# <% tp.file.title %>

## Заметки

```dataview
LIST WITHOUT ID
FROM [[]] AND #note
```

## Карты

```dataview
LIST WITHOUT ID
FROM [[]] AND #map
```

## Входящие

```dataview
LIST WITHOUT ID
FROM [[]] AND #input
```

## Исходящие

```dataview
LIST WITHOUT ID
FROM [[]] AND #output
```

## Журнал

```dataview
LIST WITHOUT ID
FROM [[]] AND #journal
```

## 0-ссылки <% tp.file.cursor() %>
