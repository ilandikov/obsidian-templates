module.exports = hanjaLinksFromTitle;

function hanjaLinksFromTitle(title) {
    const titleWords = title.split(' ');

    const hangeul = titleWords[0];
    const hanja = titleWords[1];

    switch (titleWords.length) {
        case 1:
            throw new Error('The title must have one space between hanja and hangeul!');
        case 2: 
            if (hangeul.length !== hanja.length) {
                throw new Error('The length of hanja and hangeul must be even!');
            }
            // hangeul word and hanja word of same length, normal case
            break;
        default:
            throw new Error('The title must be a block of hangeul and a block of hanja separated by a space!');
    }

    const hanjaLinks = [];

    switch (hangeul.length) {
        case 1:
            break;
        case 2: {
            hanjaLinks.push(...buildLinks(hangeul, hanja, 1));
            break;
        }
        default: {
            hanjaLinks.push(...buildLinks(hangeul, hanja, 2));
        }
    }

    return {
        links: hanjaLinks.join(' '),
        length: hangeul.length,
    };
}

/**
 * For hangeul & hanja of equal length return an array of markdown links
 * by jumping over {@param jump} characters.
 */
function buildLinks(hangeul, hanja, jump) {
    const links = [];
 
    for(let i = 0; i < hangeul.length; i = i + jump) {
        const hageulForLink = hangeul.substring(i, i + jump);
        const hanjaForLink = hanja.substring(i, i + jump)
        const hanjaLink = `[[${hageulForLink} ${hanjaForLink}]]`;
        links.push(hanjaLink);
    }

    return links;
}