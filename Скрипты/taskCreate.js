function taskCreate(description, scheduled) {
    let scheduled_postfix = '';
    if (scheduled !== undefined) {
        switch(typeof scheduled) {
            case 'number':
                scheduled_postfix += ` ⏳ ${tp.date.now(scheduled)}`;
                break;
            case 'boolean':
                if (scheduled === true) {
                    scheduled_postfix += ` ⏳ ${tp.date.now()}`;
                }
                break;
            case 'string':
                scheduled_postfix += ` ⏳ ${scheduled}`;
                break;
            default:
                break;
        }
    }
    return `- [ ] #task ${description}${scheduled_postfix}`;
}
module.exports = taskCreate;