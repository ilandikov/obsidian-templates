async function lastJournal(tp, period) {
    if (!['d', 'w'].includes(period)) {
        return;
    }

    const yyyyww = "YYYY-[Н]WW";
    const yyyymmdd = "YYYY-MM-DD";

    let daysInPeriod = 1;
    let journalFormat = yyyymmdd;
    let journalStartDate = "2023-01-01";
    if(period === 'w') {
        journalFormat = yyyyww;
        daysInPeriod = 7;
        journalStartDate = "2023-Н01";
    }
    let lastJournal = tp.date.now(journalFormat, -daysInPeriod);

    const timeToJournalStart = moment().diff(tp.date.now(yyyymmdd, 0, journalStartDate, journalFormat));
    const timeToLastJournal = moment().diff(tp.date.now(yyyymmdd, 0, lastJournal, journalFormat));
    while(timeToLastJournal < timeToJournalStart) {
        if (await tp.file.find_tfile(lastJournal) !== null) {
            break;
        }
        lastJournal = tp.date.now(journalFormat, -daysInPeriod, lastJournal, journalFormat);
    }

    if(lastJournal !== tp.date.now(journalFormat, -daysInPeriod)) {
        new Notice("Обнови журнал " + lastJournal);
    }

    return lastJournal;
}
module.exports = lastJournal;