const hanjaLinksFromTitle = require('../hanjaLinksFromTitle');

describe('hanjaLinksFromTitle tests', () => {
    function testHanjaLinks(title, expectedLength, expectedLinks) {
        const { links, length } = hanjaLinksFromTitle(title);

        expect(links).toEqual(expectedLinks);
        expect(length).toEqual(expectedLength);
    }

    it.each([
        [1, 'a 1', ''],
        [2, 'ab 12', '[[a 1]] [[b 2]]'],
        [3, 'abc 123', '[[ab 12]] [[c 3]]'],
        [4, 'abcd 1234', '[[ab 12]] [[cd 34]]'],
        [5, 'abcde 12345', '[[ab 12]] [[cd 34]] [[e 5]]'],
    ])('should provide links for %s hanja(s)', (expectedLength, title, expectedLinks) => {
        testHanjaLinks(title, expectedLength, expectedLinks);
    });

    it.each([
        ['no_spaces', 'The title must have one space between hanja and hangeul!'],
        ['only_hanja ', 'The length of hanja and hangeul must be even!'],
        [' only_hangeul', 'The length of hanja and hangeul must be even!'],
        ['two  spa', 'The title must be a block of hangeul and a block of hanja separated by a space!'],
        ['three wordss blocks', 'The title must be a block of hangeul and a block of hanja separated by a space!'],
        ['not even_hanja_and_hangeul', 'The length of hanja and hangeul must be even!'],
    ])('should throw error for "%s" title', (title, errorMessage) => {
        expect(() => hanjaLinksFromTitle(title)).toThrow(errorMessage);
    });
});