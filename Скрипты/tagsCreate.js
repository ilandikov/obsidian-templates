function tagsCreate(tp, postfix) {
    let templateFileName = tp.config.template_file.basename;
    // Check that the template starts with and underscore
    if (templateFileName.charAt(0) !== '_') {
        // Filter the case of journal creation where these files are same for some reason O_o
        switch (templateFileName) {
            case tp.date.now('YYYY-MM-DD'):
                templateFileName = "_journal_daily";
                break;
            case tp.date.now('YYYY-[Н]WW'):
                templateFileName = "_journal_weekly";
                break;
            case tp.date.now('YYYY-MM'):
                templateFileName = "_journal_monthly";
                break;
            default:
                throw new Error("tagsCreate() called on template without an undescore in the begging of the filename: " + tp.config.template_file.basename);
        }
    }

    const fileTagArray = templateFileName.split("_");
    // Remove the part from the first underscore
    fileTagArray.shift();

    // Append postfix unless unnecessary
    if (postfix !== "") {
        fileTagArray.push(postfix);
    }

    const fileTag = fileTagArray.join("/");
    const tagList = [
        "tags:",
        "  - " + fileTag,
    ];

    return tagList.join("\n");
}
module.exports = tagsCreate;