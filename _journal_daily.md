---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, '') %>
---
<%* thisWeek = tp.date.now('YYYY-[Н]WW'); -%>

# <% tp.file.title %>

<< [[<% tp.user.lastJournal(tp, 'd') %>]] | [[<% thisWeek %>]] | [[<% tp.date.now('YYYY-MM-DD', 1) %>]] >>
![[<% thisWeek %>#Внимание]]
<%*
    // Check if weekly & monthly journals were created and do so if needed
    if (!( await tp.file.find_tfile(thisWeek) )) {
        (await tp.file.create_new(tp.file.find_tfile('\_journal_weekly'),
        thisWeek,
        false,
        app.vault.getAbstractFileByPath(tp.file.folder())))
        setTimeout(null, 2000);
    }
    if (!( await tp.file.find_tfile(tp.date.now('YYYY-MM')) )) {
        (await tp.file.create_new(tp.file.find_tfile('\_journal_monthly'),
        tp.date.now('YYYY-MM'),
        false,
        app.vault.getAbstractFileByPath(tp.file.folder())))
        setTimeout(null, 2000);
    }
-%>

## Сегодня

<% tp.file.include('[[_include_routine_daily]]') %>

```tasks
not done
(scheduled before <% tp.date.now('YYYY-MM-DD', +1) %>) OR (due before <% tp.date.now('YYYY-MM-DD', +1) %>)
group by filename
hide backlink
short mode
```

## Завершенные задачи

```tasks
done on <% tp.date.now('YYYY-MM-DD') %>
hide done date
hide task count
group by filename
hide backlink
short mode
```

## Доступные задачи

```tasks
not done
no scheduled date
no due date
starts before <% tp.date.now('YYYY-MM-DD') %>
group by filename
hide backlink
```

## Заметки

```dataview
LIST WITHOUT ID file.link
FROM "Заметки"
WHERE file.cday = this.file.day
AND file.path != this.file.path
SORT file.ctime ASC 
```
