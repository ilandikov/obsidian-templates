---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, '') %>
---
<%* thisMonth = tp.date.now('YYYY-MM'); -%>

# <% thisMonth %>

<< [[<% tp.date.now('YYYY-MM', 'P-1M') %>]] | [[<% tp.date.now('YYYY') %>]] | [[<% tp.date.now('YYYY-MM', 'P1M') %>]] >>

## Внимание

- [ ] Минимум 1 задача на каждый ongoing проект

## Ощущения

## Задачи

<% tp.user.taskCreate('Провести ревизию проектов ' + thisMonth, tp.date.now('YYYY-MM-DD')) %>
<% tp.user.taskCreate('Определить объекты внимания на месяц ' + thisMonth, tp.date.now('YYYY-MM-DD')) %>
<% tp.file.include('[[_include_routine_monthly]]') %>
<% tp.user.taskCreate('Закрыть месяц ' + thisMonth, moment().format('YYYY-MM-') + moment().daysInMonth()) %>
