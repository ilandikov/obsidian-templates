---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, 'idea') %>
code: ''
review: <% tp.file.creation_date('YYYY-MM-DD') %>
invested:
  Y<% tp.file.creation_date('YYYY') %>: 0
---

# <% tp.file.title %>

Описание проекта/СС

## Цепочка создания

Описание ЦС

## Роли и рабочие продукты

| Роль | Рабочий продукт |
| ---- | --------------- |
|      |                 |

## Заметки

```dataview
LIST WITHOUT ID
FROM [[]] AND !#journal
```

## Задачи

```tasks
path includes {{query.file.path}}
not done
hide recurrence rule
hide backlink
```

## События

<% tp.file.include('[[_include_zero_links]]') %>
