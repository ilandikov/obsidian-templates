---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, '') %>
---
<%* thisWeek = tp.date.now('YYYY-[Н]WW'); -%>

# <% thisWeek %>

<%*
    let referencedMonth = '[[' + tp.date.weekday('YYYY-MM', 0) + ']]';
    if (tp.date.weekday('MM', 0) !== tp.date.weekday('MM', 6)) {
        referencedMonth += ' [[' + tp.date.weekday('YYYY-MM', 6) + ']]';
    }
-%>
<< [[<% tp.user.lastJournal(tp, 'w') %>]] | <% referencedMonth %> | [[<% tp.date.now('YYYY-[Н]WW', 'P1W') %>]] >>
![[<% tp.date.now('YYYY-MM') %>#Внимание]]

## Внимание

- [ ] То
- [ ] Се

## Задачи

<% tp.user.taskCreate('Определить объекты внимания на неделю ' + thisWeek, tp.date.now('YYYY-MM-DD')) %>
<% tp.file.include('[[_include_routine_weekly]]') %>
<% tp.user.taskCreate('Закрыть неделю ' + thisWeek, tp.date.weekday('YYYY-MM-DD', 6)) %>

## Сделано за неделю

### Понедельник

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 0) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 0) %>))
hide scheduled date
hide task count
```

### Вторник

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 1) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 1) %>))
hide scheduled date
hide task count
```

### Среда

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 2) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 2) %>))
hide scheduled date
hide task count
```

### Четверг

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 3) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 3) %>))
hide scheduled date
hide task count
```

### Пятница

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 4) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 4) %>))
hide scheduled date
hide task count
```

### Суббота

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 5) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 5) %>))
hide scheduled date
hide task count
```

### Воскресенье

```tasks
(done on <% tp.date.weekday('YYYY-MM-DD', 6) %>) OR ((not done) AND (scheduled <% tp.date.weekday('YYYY-MM-DD', 6) %>))
hide scheduled date
hide task count
```

## Написано за неделю

```dataview
LIST WITHOUT ID file.link
FROM "Заметки"
WHERE file.cday >= date(<% tp.date.weekday('YYYY-MM-DD', 0) %>)
WHERE file.cday <= date(<% tp.date.weekday('YYYY-MM-DD', 6) %>)
SORT file.ctime ASC
```
