---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, "todo") %>
---

# <% tp.file.title %>

<%*
    const titleWords = tp.file.title.split(' ');
    let linkToDate = '';
    let linkToName = '';
    if (titleWords.length > 0) {
        linkToDate = `[[${titleWords.shift()}]]`;
    }
    while (titleWords.length > 0) {
        // Add space if it is not the first loop
        if (linkToName.length > 0) {
            linkToName += ' ';
        }
        
        linkToName += `[[${titleWords.shift()}`
        // In case the name is on 2 words
        if (titleWords.length > 0) {
            linkToName += ` ${titleWords.shift()}`;
        }
        linkToName += ']]';
    }
-%>
<% tp.file.include('[[_include_zero_source]]') %><% linkToDate %> <% linkToName %>

<% tp.file.include('[[_include_zero_links]]') %><% tp.file.cursor() %>
